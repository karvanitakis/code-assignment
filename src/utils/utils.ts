import { routers } from '../components/Router/router.config';
import {
  useLocation
} from "react-router-dom";

class Utils {
  getRoute = (path: string): any => {
    return routers.filter(route => route.path === path)[0];
  };

  useQuery() {
    return new URLSearchParams(useLocation().search);
  }
}

export default new Utils();
