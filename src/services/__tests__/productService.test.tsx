import productService from "../product/productService";

describe('Ensuring that vote generator works', () => {
   let voteCount = productService.generateVoteCount();
   test('Vote count is greater than 0', () => {
    expect(voteCount).toBeGreaterThan(0);
  });
});
