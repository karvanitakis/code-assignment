export interface GetPopularProducts {
    title: string;
    description: string;
    url: string;
    votes: number;
    submitterAvatarUrl: string;
    productImageUrl: string;
    id: number;
  }
  