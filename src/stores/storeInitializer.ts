import ProductStore from './productStore';

export default function initializeStores() {
  return {
    productStore: new ProductStore()
  };
}
