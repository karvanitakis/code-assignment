import ProductStore from "../productStore";

const mockProducts = [{
    id: 1,
    title: 'TestProduct1',
    description: 'TestProduct1',
    url: '#',
    votes: 1,
    submitterAvatarUrl: 'images/avatars/daniel.jpg',
    productImageUrl: 'images/products/image-aqua.png',
},
{
    id: 2,
    title: 'TestProduct2',
    description: 'TestProduct2',
    url: '#',
    votes: 2,
    submitterAvatarUrl: 'images/avatars/kristy.png',
    productImageUrl: 'images/products/image-rose.png',
},
{
    id: 3,
    title: 'TestProduct3',
    description: 'TestProduct3',
    url: '#',
    votes: 3,
    submitterAvatarUrl: 'images/avatars/veronika.jpg',
    productImageUrl: 'images/products/image-steel.png',
}].sort((a, b) => b.votes - a.votes);

describe("ProductStore", () => {
    it("Creates new products", () => {
        const store = new ProductStore;
        store.popularProducts = mockProducts;
        expect(store.popularProducts.length).toBe(3);
        expect(store.popularProducts[0].title).toBe("TestProduct3");
        expect(store.popularProducts[1].title).toBe("TestProduct2");
        expect(store.popularProducts[2].title).toBe("TestProduct1");
    });
});

describe("ProductStore", () => {
    it("Testing sort products on load", () => {
        const store = new ProductStore;
        store.popularProducts = mockProducts;

        expect(store.popularProducts[0].title).toBe("TestProduct3");
        expect(store.popularProducts[1].title).toBe("TestProduct2");
        expect(store.popularProducts[2].title).toBe("TestProduct1");
    });
});

describe("ProductStore", () => {
    it("Testing sort products on vote change", () => {
        const store = new ProductStore;
        store.popularProducts = mockProducts;

        store.popularProductVoteUp(1);
        store.popularProductVoteUp(1);
        store.popularProductVoteUp(1);

        expect(store.popularProducts[0].title).toBe("TestProduct1");
        expect(store.popularProducts[1].title).toBe("TestProduct3");
        expect(store.popularProducts[2].title).toBe("TestProduct2");

        store.popularProductVoteUp(3);
        store.popularProductVoteUp(3);

        expect(store.popularProducts[0].title).toBe("TestProduct3");
        expect(store.popularProducts[1].title).toBe("TestProduct1");
        expect(store.popularProducts[2].title).toBe("TestProduct2");
    });
});