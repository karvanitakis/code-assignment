import { action, observable } from 'mobx';
import productService from '../services/product/productService';

class ProductStore {
  @observable popularProducts: any = [];
  @observable product: any;

  @action
  async getAllPopularProducts() {
    let result = await productService.getPopularProducts();
    this.popularProducts = result.sort((a, b) => b.votes - a.votes);
  }

  @action
  popularProductVoteUp = (id: number) => {
    let objIndex = this.popularProducts.findIndex(((obj: { id: number; }) => obj.id === id));
    this.popularProducts[objIndex].votes = this.popularProducts[objIndex].votes + 1;
    this.popularProducts = this.popularProducts.slice().sort((a: { votes: number; }, b: { votes: number; }) => b.votes - a.votes);
  }

  @action
  getPopularProductById = async (id: number) => {
    let result = await productService.getProductById(id);
    this.product = result;
  }
}

export default ProductStore;