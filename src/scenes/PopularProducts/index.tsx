import * as React from 'react';
import ProductStore from '../../stores/productStore';
import { inject, observer } from 'mobx-react';
import Stores from '../../stores/storeIdentifier';
import { GetPopularProducts } from '../../services/product/dto/getPopularProducts';
import PopularProduct from '../ProductBox/components/popularProduct';

export interface IPopularProductsProps {
    productStore?: ProductStore;
}

@inject(Stores.ProductStore)
@observer
class PopularProducts extends React.Component<IPopularProductsProps> {

    async componentDidMount() {
        await this.getAll();
    }

    async getAll() {
        await this.props.productStore!.getAllPopularProducts();
    }
    
    render() {
        const { popularProducts } = this.props.productStore!;
        return (
            <div>
                <div className="popularProdcutsWrapper">
                <h1>Popular Products</h1>

                    {popularProducts.map((pp: GetPopularProducts) => {
                        return <PopularProduct
                            key={pp.id}
                            title={pp.title}
                            description={pp.description}
                            url={pp.url}
                            votes={pp.votes}
                            allowVoting={true}
                            submitterAvatarUrl={pp.submitterAvatarUrl}
                            productImageUrl={pp.productImageUrl}
                            id={pp.id}
                            voteup={() => this.props.productStore?.popularProductVoteUp(pp.id)}
                        />
                    })}
                </div>
            </div>
        );
    }
}

export default PopularProducts;
