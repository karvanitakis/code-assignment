import * as React from 'react';
import ProductStore from "../../stores/productStore";
import { inject, observer } from "mobx-react";
import Stores from "../../stores/storeIdentifier";
import PopularProduct from '../ProductBox/components/popularProduct';
import Loading from '../../components/Loading';

export interface IProductPageProps {
    productStore?: ProductStore;
}

@inject(Stores.ProductStore)
@observer
class ProductPage extends React.Component<IProductPageProps> {
    state = {
        loading: true
    }
    
    async componentDidMount() {
        let productId = (this.props as any).match.params.id;
        await this.props.productStore?.getPopularProductById(productId)
            .then(() => this.setState({ loading: false }));
    }

    render() {
        const { product } = this.props.productStore!;

        if (this.state.loading) {
            return <Loading />;
        }
        return (
            <div>
                <div className="popularProdcutsWrapper">
                    <h1>Popular Product - {product.title}</h1>
                    <PopularProduct
                        key={product.id}
                        title={product.title}
                        description={product.description}
                        url={product.url}
                        votes={product.votes}
                        allowVoting={false}
                        submitterAvatarUrl={product.submitterAvatarUrl}
                        productImageUrl={product.productImageUrl}
                        id={product.id}
                        voteup={() => this.props.productStore?.popularProductVoteUp(product.id)}
                    />

                </div>
            </div>
        )
    }
}
export default ProductPage;