import * as React from 'react';
import Picture from '../../../components/Picture';
import '../index.scss'
import arrow from "../../../assets/images/ui/arrow.png";
import { Link } from 'react-router-dom';

export interface IProductProps {
    title: string;
    description: string;
    url: string;
    votes: number;
    allowVoting: boolean;
    submitterAvatarUrl: string;
    productImageUrl: string;
    id: number;
    voteup: () => void;
}

class PopularProduct extends React.Component<IProductProps>{
    render() {
        const { title, description, votes, allowVoting, submitterAvatarUrl, productImageUrl, voteup, id } = this.props;
        return (
            <div className="popularProduct">
                <div className="popularProduct-pictureWrapper">
                    {/* image */}
                    <Picture title={title} url={productImageUrl} />
                </div>
                <div className="popularProduct-detailsWrapper">
                    {/* rest of data */}
                    {allowVoting &&
                        <div className="votes">
                            <img title="arrow" src={arrow} onClick={voteup} alt="arrow-up" />
                            <span className="voteCount">{votes}</span>
                        </div>
                    }
                    <div className="title">
                        <Link
                            to={`/product/${id}`}>
                            <span>{title}</span>
                        </Link>
                    </div>
                    <div className="description">
                        <span>{description}</span>
                    </div>
                    <div className="submittedAvatar">
                        <span>Submitted by:</span>
                        <Picture title={title} url={submitterAvatarUrl} />
                    </div>
                </div>
            </div>
        );
    }
}

export default PopularProduct;