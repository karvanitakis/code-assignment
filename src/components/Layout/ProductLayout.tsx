import * as React from 'react';
import PopularProducts from '../../scenes/PopularProducts';

class ProductLayout extends React.Component<any> {
    render() {
        return (
            <div>
                <PopularProducts />
            </div>         
        );
    }
}
export default ProductLayout;