import LoadableComponent from './../Loadable/index';

export const productRouter: any = [
  {
    exact: true,
    path: '/',
    name: 'product',
    title: 'Product',
    component: LoadableComponent(() => import('../../components/Layout/ProductLayout')),
  },
  {
    exact:true,
    path: '/product/:id',
    name: 'productpage',
    title: 'Product Page',
    component: LoadableComponent(() => import('../../scenes/ProductPage')),
  }
];

export const routers = [...productRouter];
