import * as React from 'react';

import { Route, Switch } from 'react-router-dom';
import utils from '../../utils/utils';

const Router = () => {
  const ProductLayout = utils.getRoute('/').component;
  const ProductPage = utils.getRoute('/product/:id').component;

  return (
    <Switch>
      <Route path="/product/:id" render={(props: any) => <ProductPage {...props} />} />
      <Route path="/" render={(props: any) => <ProductLayout {...props} />} />
    </Switch>
  );
};

export default Router;
