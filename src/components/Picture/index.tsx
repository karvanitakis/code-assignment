import * as React from 'react';

export interface IPicture {
    title: string;
    url: string;
}

class Picture extends React.Component<IPicture> {
    render() {
        const { title, url } = this.props;
        return (
            <img src={require('../../assets/' + url)} alt={title} />
        );
    }
}
export default Picture;